# -*- coding: utf-8 -*-
"""
Created on Sat Oct  8 14:16:07 2016

@author: drew
"""
import sys

if "/home/starrover/PythonLibrary" not in sys.path:
    sys.path.append("/home/starrover/PythonLibrary")

import graph_structure as g_s
import heap_data_structure as hp_st

data = '_fe8d0202cd20a808db6a4d5d06be62f4_clustering1.txt'

def import_data(data):

    myfile = open(data)
    line = myfile.readline()
    num_vertices = int(line.split()[0])
    #num_edges = int(line.split()[1])

    Edges = []
    Vertices = []

    for s in xrange(0,num_vertices):
        Vertices.append(g_s.Vertex(s+1))

    line = myfile.readline()
    while line != '':

        temp = line.split(' ')
        Vertex1 = Vertices[int(temp[0])-1]
        Vertex2 = Vertices[int(temp[1])-1]
        New_Edge = g_s.Edge(Vertex1,Vertex2,float(temp[2]))

        Edges.append(New_Edge)
        if New_Edge not in Vertex1.Edges:
            Vertex1.Edges.append(New_Edge)
        if New_Edge not in Vertex2.Edges:
            Vertex2.Edges.append(New_Edge)

        line = myfile.readline()

    G = [Edges,Vertices]
    return G

G = import_data(data)

