# -*- coding: utf-8 -*-
"""
Created on Tue Oct  4 20:14:19 2016

@author: drew
"""

import sys

if "/home/drew/dev/PythonLibrary" not in sys.path:
    sys.path.append("/home/drew/dev/PythonLibrary")

import graph_structure as g_s
import numpy as np
import scipy.misc
# part 2 assignmetn

file_name = 'test1.txt'# '_fe8d0202cd20a808db6a4d5d06be62f4_clustering_big.txt'
myfile = open(file_name)

line=myfile.readline()
num_vertices = int(line.split()[0])
global num_bits
num_bits = int(line.split()[1])

hash_table = {}

template=[]
for s in range(0,num_bits):
    template.append('0')
        
def binary_string_to_decimal(string):

    number = 0    
    for s in range(1,num_bits+1):
        exponent = string[s*-1]
        if exponent == '1':
            number = number + np.power(2,s-1)
        
    return number
    
def convert_decimal_to_binary(decimal):
    
    binary = bin(decimal)
    binary = binary[2:] #remove the 0b
    num_to_add = num_bits - len(binary)
    for s in xrange(0,num_to_add):
        binary = '0'+binary
        
    return binary
    
def get_distance(number1,number2):
    
    num = number1^number2
    
    binary = convert_decimal_to_binary(num)

    counter = 0    
    for blah in binary:
        
        if blah=='1':
            counter+=1
            
    return counter
    
EMPTY_STRING=''
for s in xrange(0,num_bits):    
    EMPTY_STRING +='0'
    
global CONST_1_STRINGS 
CONST_1_STRINGS = []
for s in range(0,num_bits):
    temp = template[:]
    temp[s] = str(np.mod(int(temp[s])+1,2))
    temp = ''.join(temp)
    CONST_1_STRINGS.append(temp)
        
global CONST_2_STRINGS
CONST_2_STRINGS = []
for s in range(0,num_bits):
    for t in range(0,num_bits):
        temp = template[:]
        temp[s] = str(np.mod(int(temp[s])+1,2))
        temp[t] = str(np.mod(int(temp[t])+1,2))
        temp = ''.join(temp)
        if temp not in CONST_2_STRINGS and temp != EMPTY_STRING:
            CONST_2_STRINGS.append(temp)
                
#def add_to_strings()
                
for s in range(0,len(CONST_1_STRINGS)):
    CONST_1_STRINGS[s] = binary_string_to_decimal(CONST_1_STRINGS[s])
    
for s in range(0,len(CONST_2_STRINGS)):
    CONST_2_STRINGS[s] = binary_string_to_decimal(CONST_2_STRINGS[s])
    
def get_strings_1_bit_off(string):
    
    temp_list = []
    for blah in CONST_1_STRINGS:
        temp_list.append(blah^string)
        
    return temp_list
    
def get_strings_2_bit_off(string):
    
    temp_list = []
    for blah in CONST_2_STRINGS:
        temp_list.append(blah^string)
        
    return temp_list
    
            
#build hash table
#Initialize array
counter = 0
while line != '':
    counter = counter +1    
    line = myfile.readline()[:-2]
    line = line.split()
    line =''.join(line)
    
    #if '1 0 0 0 1 1 0 0 0 0 0 0 0 1 1 0 0 1 1 0 1 0 1 1' == line:
    #    print 'fuck'
    #    print counter
    
    #if hash_table.has_key(line):
    #    print line
    #    print counter
    if line!='':
        line_decimal = binary_string_to_decimal(line)
        hash_table[line_decimal]=[[line_decimal],1]#g_s.Vertex_Krustal(line)
    #if counter == 10003:
    #   break
    

for key in hash_table.keys():
        
    keys_to_check = get_strings_1_bit_off(key)
    
    for key_ in keys_to_check:
        if hash_table.has_key(key_) == True:
            leader1 = hash_table[key][0][0]
            leader2 = hash_table[key_][0][0]
            if leader1!=leader2:# and hash_table[key_][1]!=0:
                
                if hash_table[leader1][1] > hash_table[leader2][1]:
                    for s in xrange(0,hash_table[leader2][1]):
                        hash_table[leader1][0].append(hash_table[leader2][0][s])
                        hash_table[leader1][1]+=1
                        if s!=0:
                            hash_table[hash_table[leader2][0][s]]=[[leader1],0]
                    hash_table[leader2]=[[leader1],0]
                else:
                    for s in xrange(0,hash_table[leader1][1]):
                        hash_table[leader2][0].append(hash_table[leader1][0][s])
                        hash_table[leader2][1]+=1
                        if s!=0:
                            hash_table[hash_table[leader1][0][s]]=[[leader2],0]
                    hash_table[leader1]=[[leader2],0]

for key in hash_table.keys():
        
    keys_to_check = get_strings_2_bit_off(key)
    
    for key_ in keys_to_check:
        if hash_table.has_key(key_) == True:
            leader1 = hash_table[key][0][0]
            leader2 = hash_table[key_][0][0]
            if leader1!=leader2:# and hash_table[key_][1]!=0:
                
                if hash_table[leader1][1] > hash_table[leader2][1]:
                    for s in xrange(0,hash_table[leader2][1]):
                        hash_table[leader1][0].append(hash_table[leader2][0][s])
                        hash_table[leader1][1]+=1
                        if s!=0:
                            hash_table[hash_table[leader2][0][s]]=[[leader1],0]
                    hash_table[leader2]=[[leader1],0]
                else:
                    for s in xrange(0,hash_table[leader1][1]):
                        hash_table[leader2][0].append(hash_table[leader1][0][s])
                        hash_table[leader2][1]+=1
                        if s!=0:
                            hash_table[hash_table[leader1][0][s]]=[[leader2],0]
                    hash_table[leader1]=[[leader2],0]

counter = 0 # count number of leaders
check_sum=0
for key in hash_table.keys():
    
    if hash_table[key][0][0]==key:
        counter+=1
        check_sum = check_sum+hash_table[key][1]
        

    

        
    