# -*- coding: utf-8 -*-
"""
Created on Tue Oct 11 19:31:42 2016

@author: drew
"""

# -*- coding: utf-8 -*-
"""
Created on Thu Aug 25 18:36:54 2016

@author: starrover
"""

import sys
sys.path.append("/home/drew/Dev/PythonLibrary")

from heap_data_structure import *
import graph_structure as g_s

import Bellman_ford_algorithm as bf
from graph_algorithms import *

'''
class BF_Vertex(g_s.Vertex):
    
    def __init__(self,number):
        
        g_s.Vertex.__init__(self,number)
        self.adj_trans = 0
        
    def set_adj_trans(self,value):       
        self.adj_trans = value
'''

data = "data_3.txt"
#data = "test_data.txt"

def import_data(data):

    myfile = open(data)
    line = myfile.readline()
    num_vertices = int(line.split()[0])
    #num_edges = int(line.split()[1])

    Edges = []
    Vertices = []

    for s in xrange(0,num_vertices):
        Vertices.append(g_s.Vertex(s+1))

    line = myfile.readline()
    while line != '':

        temp = line.split(' ')
        Vertex1 = Vertices[int(temp[0])-1]
        Vertex2 = Vertices[int(temp[1])-1]
        New_Edge = g_s.Edge(Vertex1,Vertex2,float(temp[2]))

        Edges.append(New_Edge)
        if New_Edge not in Vertex1.Edges:
            Vertex1.Edges.append(New_Edge)
        if New_Edge not in Vertex2.Edges:
            Vertex2.Edges.append(New_Edge)

        line = myfile.readline()

    G = [Edges,Vertices]
    return G

Graph = import_data(data)


def add_phantom_vertex(Graph):
    
    num_vertices = len(Graph[1])
    
    New_Vertex = g_s.Vertex(num_vertices+1)
    Graph[1].append(New_Vertex)
    
    for s in range(0,num_vertices):

        New_Edge = g_s.Edge(New_Vertex,Graph[1][s],0)
        Graph[0].append(New_Edge)

        if New_Edge not in New_Vertex.Edges:
            New_Vertex.Edges.append(New_Edge)
        #if New_Edge not in Graph[1][s].Edges:

    return Graph


original_num_edges = len(Graph[0])
Graph = add_phantom_vertex(Graph)
A,B = bf.Bellman_ford_algorithm(Graph,len(Graph[1]))#len(Graph[1]))

Graph = bf.transformation(Graph,A,original_num_edges)

#X,distances = dykstra(Graph,1)

blah = Heap([])
for source_vertex in Graph[1]:
    #source_vertex = Graph[1][4]
    
    X,distances = dykstra(Graph,source_vertex.number) #source vector v.number to X[-1].number
    
    
    for i in range(0,len(Graph[1])):
        if Graph[1][i].number!= source_vertex.number:
            distance = distances[-i]-A[-1,source_vertex.number-1]+A[-1,X[-i].number-1]
            #print distance
            blah.insert(distance)
    
   #blah.insert(distances[0])


                
        
                
        

        





    

    