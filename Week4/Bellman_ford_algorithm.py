# -*- coding: utf-8 -*-
"""
Created on Tue Oct 11 23:38:30 2016

@author: drew

BELLMAN - FORD ALgorithm

"""
import sys
sys.path.append("/home/drew/Dev/PythonLibrary")

import graph_structure as g_s
import heap_data_structure as hp

class Heap_element(g_s.Vertex):
    
    def __init__(self,key):
        self.heap_key = key
        self.atr1 = 0
        self.atr2 = 0
        
    def set_atr1(self,atr1):
        self.atr1 = atr1
    def set_atr2(self,atr2):
        self.atr2 = atr2
        
        
import numpy as np

def Bellman_ford_algorithm(Graph,source_vertex_num):

    check_for_neg_loops = True
    if check_for_neg_loops:
        extra_iteration = 1
    else:
        extra_iteration = 0
        
    INFINITY = 100000000
    
    V = Graph[1] #vertices
    num_vertices = len(V)
    
    #source_vertex = 1
    
    A = np.zeros((num_vertices+extra_iteration,num_vertices))
    A[0,source_vertex_num-1] = 0
    B = np.zeros(num_vertices)
    B[source_vertex_num-1]=source_vertex_num
    
    for s in xrange(0,num_vertices):
        if s != source_vertex_num-1:
            A[0,s] = INFINITY
    
    for i in range(1,num_vertices+extra_iteration):
        for j in xrange(0,num_vertices):
            
            temp_list=hp.Heap([])
            temp_list.Objects_flag = 1
            new_object = Heap_element(A[i-1,j])
            temp_list.insert(new_object)
            new_object.set_atr1(V[j].number)
            
            for e in V[j].Edges:
                
                if e.get_head().number == V[j].number:
                    new_object = Heap_element(A[i-1,e.get_tail().number-1]+e.length)   
                    #new_object.set_atr2(e.length)
                    new_object.set_atr1(e.get_tail().number)
                    
                    temp_list.insert(new_object)
                    
                    #if e.length < 0:
                    #    print e.length
    
            min_object = temp_list.get_min()        
            A[i,j] = min_object.heap_key
            if min_object.atr1!=V[j].number:
                B[j] = min_object.atr1
      
      
    if check_for_neg_loops == True:
        neg_flag = 0    
        for s in xrange(0,num_vertices):
            
            if A[-1,s]!=A[-2,s]:
                neg_flag = 1
                
                
    print neg_flag
                
    return A,B
            
#### Create phantom vertex
'''       
def create_phantom_vertex(Graph):
    
    Graph[1].append(Vertex(len(Graph(1)+1)))    
'''

def reconstruct_path(destination_vertex,B):
    
    vert_path = []
    num_vertices = len(B)
    
    vert_path.append(destination_vertex)
    next_vertex = B[destination_vertex-1]
    
    for s in range(0,num_vertices):
        if next_vertex != vert_path[-1]:
            vert_path.append(next_vertex)
        next_vertex = B[next_vertex-1]
    vert_path.reverse()
    
    return vert_path
    
def transformation(Graph,A,original_num_edges):
    
    #final_vertex = Graph[1][-1]    
    
    for e in Graph[0]:
        
        e.length = e.length + A[-1,e.vertex1.number-1]-A[-1,e.vertex2.number-1]
        
    Graph[1].pop(-1)
    
    Graph[0] = Graph[0][0:original_num_edges]
          
    return Graph
    
        
        

    