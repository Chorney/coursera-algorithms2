# -*- coding: utf-8 -*-
"""
Created on Fri Oct 14 17:09:56 2016

@author: drew
"""
import sys
import math
sys.path.append("/home/drew/Dev/PythonLibrary")
import heap_data_structure as hp



f = open("data_.txt")
data = f.readlines()
f.close()

#data.pop(0)

class Vertex:
    
    def __init__(self,number,x,y):
        
        self.x = x
        self.y = y
        self.number = number
        self.Edges = []
        
class Edge:

    def __init__(self,v1,v2):
        
        self.vertex1 = v1
        self.vertex2 = v2
        self.edge_length = 0

    def set_edge_length(self,edge_length):        
        self.edge_length = edge_length
    def get_edge_length(self,edge_length):
        return self.edge_length
        
#def get_distance()
        
x_ = []
y_ = []

Vertex_list = []
Vertex_list_left = []
Vertex_list_right = []

test_x=[]
test_y=[]

counter = 1
counter_right = 1
for line in data:
    x = float(line.split()[0])
    y = float(line.split()[1])
        
    New_Vertex = Vertex(counter,x,y)      
    Vertex_list.append(New_Vertex)
    
    if x < 24600:
        Vertex_list_left.append(New_Vertex)
    else:
        New_Vertex_right = Vertex(counter_right,x,y)
        Vertex_list_right.append(New_Vertex_right)
        counter_right += 1
    
    x_.append(x)
    y_.append(y)
    
    if counter in [13,12,11,16,14,15]:
        test_x.append(x)
        test_y.append(y)
    
    counter += 1
    
import matplotlib.pyplot

matplotlib.pyplot.plot(x_,y_,'.')
matplotlib.pyplot.plot(test_x,test_y,'x')


def TSP(Vertex_list,setsize):

    edge_hash = {}
    edge_hash['1','1']=0
    #Edge_list = []  
    for v1 in Vertex_list:
        for v2 in Vertex_list:
            if v2.number != v1.number: # >
                
                
                dist = math.sqrt((v1.x-v2.x)**2+(v1.y-v2.y)**2)
                edge_hash[str(v1.number),str(v2.number)] = dist
                
                
    
       
    '''     
    len
    (1,2)
    (1,3)
    ...
    (1.25)
    (1,2,3) (1,2,4) (1,2,5) .... (1,2,n)
    (1,3,4)
    '''
    
    Sets = [['1']]
    A={}
    A[Sets[0][0],'1'] = 0
    
    #setsize = 4
    old_sets = Sets[:]
    
    for t in range(2,setsize+1):
    
        new_sets = []
        
        for st in old_sets:
            temp = st[:]
            inc = int(st[-1])+1
            for s in range(inc,setsize+1):
                #print s
                temp2=temp[:]
                temp2.append(str(s))
                new_sets.append(temp2)
                
                temp2_key = ','.join(temp2)
                A[temp2_key,'1'] = 1000000
                
            #print new_sets
                
        old_sets = new_sets[:]           
        Sets.append(new_sets)
            
    
    
    
    
    #Sets = build_sets(Sets)
    
    
    Sets.pop(0)
    #num_vertices = len(vertex_list)
    '''
    for sets_length_m in Sets:
    #for set_length in range(2,num_vertices+1):
        
        set_length = len(sets_length_m[0])
        for st in sets_length_m:
        #for size(S) = set_length that contains 1:
            for j in st: 
                if j!='1':    
                    st_key = ','.join(st)
                    st_minus_j = st[:]
                    st_minus_j.remove(j)
                    try:
                        st_minus_j_key = ','.join(st_minus_j)
                    except:
                        st_minus_j_key = st_minus_j[0]
                    temp_heap = hp.Heap([])
                    for k in st_minus_j:
                        value = A[st_minus_j_key,k]+edge_hash[k,j]
                        temp_heap.insert(value)
                    
                    
                    A[st_key,j] = temp_heap.get_min()
    '''
    #final_element_key = Sets[-1][0]
    final_element_key = 0
    
    return A,edge_hash,final_element_key

#left vertices
'''
setsize = 13
#start 13
switch_num = 11
Vertex_list_left_temp = Vertex_list_left[:]
temp = Vertex_list_left[switch_num-1]
Vertex_list_left_temp[switch_num-1] = Vertex_list_left_temp[0]
Vertex_list_left_temp[0] = temp
Vertex_list_left_temp[0].number = 1
Vertex_list_left_temp[switch_num-1].number = switch_num
A_13,edge_hash13,final_element_list_13 = TSP(Vertex_list_left_temp,setsize)
final_element_key = ','.join(final_element_list_13)
'''

distance_ = {}

distance_['13','12'] = 13347.76639205046#A_13[final_element_key,'12']
distance_['13','11'] = 13214.953340970744#A_13[final_element_key,'11']

distance_['12','13'] = 13347.766392050462#A_13[final_element_key,'13']
distance_['12','11'] = 13250.051702872162#A_13[final_element_key,'11']

distance_['11','12'] = 13250.051702872162#A_13[final_element_key,'12']
distance_['11','13'] = 13214.953340970744#A_13[final_element_key,'13']


#right vertices



'''
setsize = len(Vertex_list_right)
switch_num = 2
Vertex_list_right_temp = Vertex_list_right[:]
temp = Vertex_list_right[switch_num-1]
Vertex_list_right_temp[switch_num-1] = Vertex_list_right_temp[0]
Vertex_list_right_temp[0] = temp
Vertex_list_right_temp[0].number = 1
Vertex_list_right_temp[switch_num-1].number = switch_num
A_13,edge_hash13,final_element_list_13 = TSP(Vertex_list_right_temp,setsize)
final_element_key = ','.join(final_element_list_13)
'''

distance_1_2 = 9522.954178051357#A_13[final_element_key,'2']
distance_['14','15'] = distance_1_2
distance_['15','14'] = distance_1_2

distance_1_3 = 9476.656665451079#A_13[final_element_key,'3']
distance_['14','16'] = distance_1_3
distance_['16','14'] = distance_1_3

distance_2_3 = 9204.338868528412#A_13[final_element_key,'3']
distance_['15','16'] = distance_2_3
distance_['16','15'] = distance_2_3

setsize = 25
A,edge_hash,final_element_list = TSP(Vertex_list,setsize)
#final_element_key = ','.join(final_element_list)

#value1 = distance_['13','12']+distance_['14','15']
#value2 = distance_['13','12']+distance_['14','16']
#value3 = distance_['13','12']+distance_['15','16']

#value4 = distance_[]

pairs_left = [['12','13'],['11','13'],['11','12']]
pairs_right = [['14','15'],['14','16'],['15','16']]

values = []
for pair_left in pairs_left:
    for pair_right in pairs_right:
        
        distance_value_1 = distance_[pair_left[0],pair_left[1]]+distance_[pair_right[0],pair_right[1]]+edge_hash[pair_left[0],pair_right[0]]+edge_hash[pair_left[1],pair_right[1]]
        distance_value_2 = distance_[pair_left[0],pair_left[1]]+distance_[pair_right[0],pair_right[1]]+edge_hash[pair_left[1],pair_right[0]]+edge_hash[pair_left[0],pair_right[1]]

        values.append(distance_value_1)
        values.append(distance_value_2)
        
final_min = values.sort()[0]

#######
#CORRECT ANSWER IS 26442
######
'''
#Sets[-]
heap_temp = hp.Heap([])
for i in range(2,setsize+1):
    blah = ','.join(final_element_key)
    heap_temp.insert(A[blah,str(i)]+edge_hash[str(i),'1'])

min_value = heap_temp.get_min()
'''



#myfile = open('output.txt','w')
#myfile.write(str(min_value))
#myfile.close()
