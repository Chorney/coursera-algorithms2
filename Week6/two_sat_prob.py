# -*- coding: utf-8 -*-
"""
Created on Wed Oct 19 17:00:51 2016

@author: drew
two sat problem

need to run B-F-S then Depth-F-S twice to find the strongly connected compononets
"""

import PythonLibrary.graph_structure as g_s


def import_data(data):

    myfile = open(data)
    line = myfile.readline()
    num_vertices = int(line.split()[0])
    #num_edges = int(line.split()[1])

    Edges = []
    Vertices = []

    for s in xrange(0,num_vertices):
        Vertices.append(g_s.Vertex(s+1))
    for s in xrange(0,num_vertices):
        Vertices.append(g_s.Vertex((s+1)*-1))

    line = myfile.readline()
    while line != '':
        temp = line.split(' ')

        vert1_number = int(temp[0])
        vert2_number = int(temp[1])
        
        Vertex1 = get_vertex(vert1_number*-1,Vertices)
        Vertex2 = get_vertex(vert2_number,Vertices)
        New_Edge1 = g_s.Edge(Vertex1,Vertex2)#,float(temp[2]))
        
        Edges.append(New_Edge1)
        if New_Edge1 not in Vertex1.Edges:
            Vertex1.Edges.append(New_Edge1)
        if New_Edge1 not in Vertex2.Edges:
            Vertex2.Edges.append(New_Edge1)
            
        Vertex1 = get_vertex(vert1_number,Vertices)
        Vertex2 = get_vertex(vert2_number*-1,Vertices)     
        New_Edge2 = g_s.Edge(Vertex2,Vertex1)
        
        Edges.append(New_Edge2)
        if New_Edge2 not in Vertex1.Edges:
            Vertex1.Edges.append(New_Edge2)
        if New_Edge2 not in Vertex2.Edges:
            Vertex2.Edges.append(New_Edge2)

        line = myfile.readline()

    G = [Edges,Vertices]
    return G
    
    
def get_vertex(vertex_number,Vertices):
    
    #Vertices = Graph[1]
    
    num_vertices = len(Vertices)/2
    
    if vertex_number > 0:
        return Vertices[vertex_number-1]
    else:
        return Vertices[num_vertices+vertex_number*-1-1]

data = "sat6.txt"
Graph = import_data(data)

def breath_first_search(Vertices):
    
    Leaders = []
    
    #Vertices = Graph[1]    
    
    for v in Vertices:        
        if v.searched == 0:
            Leaders.append([])
            vertex_queue = []
            vertex_queue.append(v)
            v.searched = 1
               
            while len(vertex_queue) > 0:
                v_ = vertex_queue.pop(0)
                Leaders[-1].append(v_)
                
                for e in v_.Edges:
                    if e.get_other_vertex(v_).searched == 0:
                        vertex_queue.append(e.get_other_vertex(v_))
                        e.get_other_vertex(v_).searched = 1
                       
    return Leaders

#global_num_vertex = len(Graph[1])

def reset_vertices(V):
    
    for v in V:
        v.searched = 0

def DFS(Vertices,vertex_number,global_num_vertex,Leaders,rev=False):
        
    V = get_vertex(vertex_number,Vertices)

    if V.searched == 0:
        V.searched =1
    if rev==False:
        Leaders[-1].append(V)
    
    
    flag = 0
    #for num in G[vertex-1].attached_vertices:
    for e in V.Edges:
        
        if e.get_head(rev).number != V.number:
            attached_vertex = e.get_head(rev)
            if attached_vertex.searched == 0:
                #print num
                attached_vertex.searched = 1
                flag = 1
                Vertices,global_num_vertex,Leaders = DFS(Vertices,attached_vertex.number,global_num_vertex,Leaders,rev)
    
    #if flag==0:
    V.finishing_time = global_num_vertex
    global_num_vertex = global_num_vertex + 1
        
    #if flag==0:
    return Vertices, global_num_vertex, Leaders
    

#def find_sccs(Graph):
L = breath_first_search(Graph[1])
reset_vertices(Graph[1])


def get_sccs(Vertex_Set):

    #Vertex_Set = L[0]
    
    #reverse DFS on set to get finishing times
    global_num_vertex = 1
    #global global_num_vertex
    dummy_leaders = []
    for v in Vertex_Set:
        if v.searched==0:
            Vertices,global_num_vertex,dummy_leaders = DFS(Graph[1],v.number,global_num_vertex,dummy_leaders,rev=True)
    
    
    #sort vertices according to finishing times based on the first DFS run
    reset_vertices(Vertex_Set)
    Vertex_Set.sort(key=lambda x: x.finishing_time, reverse=True)
    
    #forward DFS to find SCC Leaders
    Leaders = []
    for v in Vertex_Set:
        if v.searched==0:
            Leaders.append([])
            Vertices,global_num_vertex,Leaders = DFS(Graph[1],v.number,global_num_vertex,Leaders,rev=False)
            
               
    return Leaders

def check_if_scc_is_bad(vertex_list):
    #this check is very bad, could be optimised with 
    for v in vertex_list:
        for v_ in vertex_list:
            if v.number == -1*v_.number:
                print v.number
                return True
    
    return False


bool_value = False
for l in L:
    Leaders = get_sccs(l)
    
    for l_ in Leaders:
        
        bool_value = check_if_scc_is_bad(l_)
        
        if bool_value == True:
            print "it is bad"
            break
        
    if bool_value == True:
        break
    
"ANSWER IS 101100, sat1 - good, sat2 - bad, sat3-good, sat4-good,sat5-bad,sat6-bad"

    
