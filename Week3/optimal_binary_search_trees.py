# -*- coding: utf-8 -*-
"""
Created on Thu Oct 20 18:44:05 2016

@author: drew
"""

import numpy as np

#w1=.2,w2=.05,w3=.17,w4=.1,w5=.2,w6=.03,w7=.25.

w0=0.0
w1=0.2
w2=0.05
w3=0.17
w4=0.1
w5=0.2
w6=0.03
w7=0.25 

list_ = [w1,w2,w3,w4,w5,w6,w7]
num_elem = len(list_)
list_.sort()

'''
A = np.zeros([num_elem+1,num_elem+1])

for s in range(0,num_elem):
    for i in range(1,num_elem+1):

        min_value = 1000000        
        for r in range(i,i+s+1):
           
            sum_ = 0.0
            for k in range(i,i+s+1):
                sum_ = sum_ + list_[k]
            value = sum_ + A[i,r-1]+A[r+1,i+s]
            
            if value < min_value:
                min_value = value
                
        A[i,i+s] = value
'''
def sum_probabilites(probs,index1,index2):
    
    sum_ = 0.0
    for i in range(index1,index2+1):
        sum_ = sum_ + probs[i]
        
    return sum_

#compute_cost(0,n-1)
    
def compute_cost(i,j,list_):   # 0<= i,j <=n-1

    if i < j:

        values = []
        for r in range(i,j+1):    
            values.append(sum_probabilites(list_,i,j)+compute_cost(0,r-1,list_)+compute_cost(r+1,j,list_))
            
        return np.min(values)
    elif i==j:
        return list_[i]   
    else:
        return 0.0
        
value = compute_cost(0,num_elem-1,list_)
