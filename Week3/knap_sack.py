# -*- coding: utf-8 -*-
"""
Created on Sun Oct  9 13:49:41 2016

@author: drew
"""
''''
algorothm needs some debuggin for the _6dfda29 file the answer changes depending on if sorted. Algorthm works for test cases, and works
# for the big data set
''''

import numpy as np
import sys
import time

sys.setrecursionlimit(4000)


class Item:
    
    list = []
    num_items = 0
    knapsack_size = 0
    
    @classmethod
    def add_new_item(cls,self):
        cls.list.append(self)
        
    @classmethod
    def sort_list(cls,default=False):
        cls.list.sort(key=lambda x: x.weight, reverse=default)
    
    def __init__(self,weight,value):
        self.weight = weight
        self.value = value
        Item.add_new_item(self)


file_name = '_6dfda29c18c77fd14511ba8964c2e265_knapsack1.txt'
#file_name = 'knapsack_big.txt'

def import_data(file_name):
    
    
    myfile = open(file_name)
    
    line = myfile.readline()
    knapsack_size = int(line.split()[0])
    number_of_items = int(line.split()[1])
    line = myfile.readline()
    
    Item.num_items = number_of_items
    Item.knapsack_size = knapsack_size
    
    while line != '':
        
    
        weight = int(line.split()[1])
        value = int(line.split()[0])
        Item(weight,value)
        
        line = myfile.readline()
        
    myfile.close()
    
#import_data(file_name)

#Item.sort_list()


Item(15,101)
Item(7,12)
Item(2,13)
Item(5,50)
Item(12,7)
Item(22,1000)
#Item(2,25)
#Item(5,1000)
#Item(5,1000)
#Item(10,3)
#Item(7,10)
Item.knapsack_size = 21
Item.num_items = 6


Item.sort_list(False)

max_value_arr = np.zeros(Item.num_items)
max_weight_arr = np.zeros(Item.num_items)
counter1 = 0
counter2 = 0
counter3 = 0
#global counter

hash_table = {}

def do_it_recursively(n,W):
    global counter1
    global counter2
    global counter3
    global hash_table
    global max_value_arr
    #counter += 1
    #if np.mod(counter,100) > 99:
    #    print max_value_arr

    key_name = str(n)+str(W)
        
    
    if hash_table.has_key(key_name)==False:
        #print n
        counter1+=1
        if n > 0:
            
            value_1,weight1 = do_it_recursively(n-1,W)
            #weight_1_temp =         
    
            W_red  = W - Item.list[n].weight            
            if W_red < 0:
                value_2,weight2 = [0,0]
                counter3+=1
            else:
                value_2,weight2 = do_it_recursively(n-1,W_red)
            value_2 = value_2 + Item.list[n].value
    
            #if np.max([value_1_temp,value_2_temp]) <= W:
            if value_2 > value_1 and weight2 + Item.list[n].weight <= W:
                max_value_arr[n] = value_2
                #if hash_table.__len__() > 15400000:
                #    hash_table.__delitem__(hash_table.keys()[0])
                hash_table[key_name] = [value_2,weight2+Item.list[n].weight]
                return value_2,weight2+Item.list[n].weight
            else:
                max_value_arr[n] = value_1
                #if hash_table.__len__() > 15400000:
                #    hash_table.__delitem__(hash_table.keys()[0])
                hash_table[key_name] = [value_1,weight1]
                return value_1,weight1    
    
        else:
            
            if Item.list[0].weight <= W:
                #if hash_table.__len__() > 15400000:
                #    hash_table.__delitem__(hash_table.keys()[0])
                
                hash_table[key_name] = [Item.list[0].value,Item.list[0].weight]
                
                return [Item.list[0].value,Item.list[0].weight]
            else:
                #if hash_table.__len__() > 15400000:
                #    hash_table.__delitem__(hash_table.keys()[0])
                hash_table[key_name] = [0,0]
                return [0,0]
                    
                        
                        #max_value_arr[0] = Item.list[0].value     
                        #max_weight_arr[0] = Item.list[0].weight
                    #else:
                        #max_value_arr[0] = 0
                
    else:
        counter2 += 1
        return hash_table[key_name]
        

t0=time.time()
do_it_recursively(Item.num_items-1,Item.knapsack_size)
tf=time.time()
elapsed_time = (tf-t0)/60.0

